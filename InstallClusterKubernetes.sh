#!/bin/bash

#
# ATENÇÃO, ESTE SCRIPT PRECISA SER ATUALIZADO. VC PODE INSTALAR TANTO UM NÓ MASTER ÚNICO COMO UM NÓ MASTER EM CLUSTER, COM MAIS DE 1 MASTER.
#

#Variables
#nodeName=${1}

# echo =============================================
# echo CHANGE NAME HOSTNAME
# echo =============================================

# hostname ${nodeName}
# echo ${nodeName} > /etc/hostname

echo =============================================
echo INSTALLING DOCKER
echo =============================================

apt-get update -y
apt-get install docker.io -y 
service docker start
systemctl enable docker
usermod -a -G docker ubuntu

echo =============================================
echo CONFIGURE SYSTEM.D
echo =============================================

cat > /etc/docker/daemon.json <<EOF
 {
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

mkdir -p /etc/systemd/system/docker.service.d

systemctl daemon-reload
systemctl restart docker

checkSystemD=$(docker info | grep -i cgroup)

cat > /etc/modules-load.d/k8s.conf <<EOF
br_netfilter
ip_vs
ip_vs_rr
ip_vs_sh
ip_vs_wrr
nf_conntrack_ipv4
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sysctl --system

#SWAP off
swapoff -a

#MÓDULOS NECESSARIOS
modprobe br_netfilter ip_vs_rr ip_vs_wrr nf_conntrack_1pv4 ip_vs

#INSTALANDO O K8S
apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

#IMAGES DEFAULT K8S
kubeadm config images pull

#DESLIGANDO SWAP

swapoff -a

#INICIALIZANDO CLUSTER COM INIT - PADRÃO PARA CLUSTER PADRÃO, SEM MAIS NÓS MASTERS
#kubeadm init

#INICIALIZANDO CLUSTER COM INIT - PADRÃO PARA CLUSTER STACKED - COM MAIS DE UM NÓS MASTER - STACKED - https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/

#kubeadm init --control-plane-endpoint "k8s-haproxy:6443" --upload-certs

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

#DRIVER DE REDE WEAVE NET
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

# if [ ${checkSystemD} == '' ]; then
#     echo =============================================
#     echo -e "\033[31;7mSYSTEM.D CONFIG FAILED. CHECK PLEASE\e[0m";
#     echo ============================================= 
#     exit 1
# fi

# checkModules=$(cat /etc/modules-load.d/k8s.conf)

# if [ ${checkModules} == '' ]; then
#     echo =============================================
#     echo -e "\033[31;7mCONFIG MODULES FAILED. CHECK PLEASE\e[0m";
#     echo ============================================= 
#     exit 1
# else
#     echo =============================================
#     echo -e "\033[32;7mINSTALL SUCCESSFUL.\e[0m";
#     echo ============================================= 
#     exit 0
# fi

