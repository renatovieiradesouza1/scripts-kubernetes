#!/bin/bash

#Variables
#nodeName=${1}

# echo =============================================
# echo CHANGE NAME HOSTNAME
# echo =============================================

# hostname ${nodeName}
# echo ${nodeName} > /etc/hostname

echo =============================================
echo INSTALLING DOCKER
echo =============================================

apt-get update -y
apt-get install docker.io -y 
service docker start
systemctl enable docker
usermod -a -G docker ubuntu

echo =============================================
echo CONFIGURE SYSTEM.D
echo =============================================

cat > /etc/docker/daemon.json <<EOF
 {
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

mkdir -p /etc/systemd/system/docker.service.d

systemctl daemon-reload
systemctl restart docker

checkSystemD=$(docker info | grep -i cgroup)

#INSTALANDO O K8S
apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

#SWAP off
swapoff -a





